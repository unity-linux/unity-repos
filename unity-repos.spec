%define		raw_version	31

Summary:        Unity-Linux package repositories
Name:           unity-repos
Version:        31
Release:        0.1
License:        MIT
Group:          System Environment/Base
URL:            https://pagure.io/fedora-repos/
# tarball is created by running make archive in the git checkout
# Just grab Recent Keys a repos from Fedora
Source:    %{name}-%{version}.tar.xz

#Fedora Repos
Source1: 	https://src.fedoraproject.org/rpms/fedora-repos/raw/master/f/RPM-GPG-KEY-fedora-%{version}-primary
Source2: 	https://src.fedoraproject.org/rpms/fedora-repos/raw/master/f/RPM-GPG-KEY-fedora-%{raw_version}-primary
Source3:	https://src.fedoraproject.org/rpms/fedora-repos/raw/master/f/fedora.repo
Source4:	https://src.fedoraproject.org/rpms/fedora-repos/raw/master/f/fedora-rawhide.repo
Source5:	https://src.fedoraproject.org/rpms/fedora-repos/raw/master/f/fedora-updates.repo
Source6:	https://src.fedoraproject.org/rpms/fedora-repos/raw/master/f/fedora-cisco-openh264.repo
Source7:	https://src.fedoraproject.org/rpms/fedora-repos/raw/master/f/archmap

#RPMFusion Repos
#Source8:        https://pkgs.rpmfusion.org/cgit/free/rpmfusion-free-release.git/plain/RPM-GPG-KEY-rpmfusion-free-fedora-%{version}-primary
#Source9:	https://pkgs.rpmfusion.org/cgit/free/rpmfusion-free-release.git/plain/RPM-GPG-KEY-rpmfusion-free-fedora-%{raw_version}-primary
#Source10:       https://pkgs.rpmfusion.org/cgit/free/rpmfusion-free-release.git/plain/rpmfusion-free.repo
#Source11:       https://pkgs.rpmfusion.org/cgit/free/rpmfusion-free-release.git/plain/rpmfusion-free-tainted.repo
#Source12:       https://pkgs.rpmfusion.org/cgit/free/rpmfusion-free-release.git/plain/rpmfusion-free-updates.repo
#Source13:	https://pkgs.rpmfusion.org/cgit/free/rpmfusion-free-release.git/plain/rpmfusion-free-rawhide.repo

Provides:       unity-repos(%{version})
Requires:	unity-gpg-keys
Obsoletes:      fedora-repos
#Obsoletes:      rpmfusion-free-release
Provides:       fedora-repos
BuildArch:      noarch

%description
Unity-Linux package repository files for yum and dnf along with gpg public keys

%package rawhide
Summary:        Rawhide repo definitions
Requires:       unity-repos = %{version}-%{release}
Requires:	unity-gpg-keys

%description rawhide
This package provides the rawhide repo definitions.

%package -n unity-gpg-keys
Summary:        Unity-Linux RPM keys
Obsoletes:      fedora-gpg-keys
Provides:       fedora-gpg-keys
Provides:       unity-gpg-keys

%description -n unity-gpg-keys
This package provides the RPM signature keys.

%prep
%setup -q

%build


%install
#%{__cp} ../../*rpmfusion* .
%{__cp} ../../*fedora* .
%{__cp} ../../archmap .

# Install the keys
install -d -m 755 $RPM_BUILD_ROOT/etc/pki/rpm-gpg
install -m 644 RPM-GPG-KEY* $RPM_BUILD_ROOT/etc/pki/rpm-gpg/

# Link the primary/secondary keys to arch files, according to archmap.
# Ex: if there's a key named RPM-GPG-KEY-fedora-19-primary, and archmap
#     says "fedora-19-primary: i386 x86_64",
#     RPM-GPG-KEY-fedora-19-{i386,x86_64} will be symlinked to that key.
pushd $RPM_BUILD_ROOT/etc/pki/rpm-gpg/
for keyfile in RPM-GPG-KEY*; do
    key=${keyfile#RPM-GPG-KEY-} # e.g. 'fedora-20-primary'
    arches=$(sed -ne "s/^${key}://p" $RPM_BUILD_DIR/%{name}-%{version}/archmap) \
        || echo "WARNING: no archmap entry for $key"
    for arch in $arches; do
        # replace last part with $arch (fedora-20-primary -> fedora-20-$arch)
        ln -s $keyfile ${keyfile%%-*}-$arch # NOTE: RPM replaces %% with %
    done
done
# and add symlink for compat generic location
ln -s RPM-GPG-KEY-fedora-%{version}-primary RPM-GPG-KEY-%{version}-fedora
popd

install -d -m 755 $RPM_BUILD_ROOT/etc/yum.repos.d

#for file in rpmfusion*repo ; do
#  install -m 644 $file $RPM_BUILD_ROOT/etc/yum.repos.d
#done

for file in fedora*repo unity*repo ; do
  install -m 644 $file $RPM_BUILD_ROOT/etc/yum.repos.d
done

#Make sure rawhide is disabled
sed -i 's!enabled=.*!enabled=0!g' $RPM_BUILD_ROOT/etc/yum.repos.d/*rawhide.repo

%files
%defattr(-,root,root,-)
%dir /etc/yum.repos.d
#%config(noreplace) /etc/yum.repos.d/rpmfusion*.repo
%config(noreplace) /etc/yum.repos.d/fedora*.repo
%config(noreplace) /etc/yum.repos.d/unity*.repo

%files rawhide
%defattr(-,root,root,-)
%config(noreplace) /etc/yum.repos.d/fedora-rawhide.repo
#%config(noreplace) /etc/yum.repos.d/rpmfusion-free-rawhide.repo

%files -n unity-gpg-keys
%dir /etc/pki/rpm-gpg
/etc/pki/rpm-gpg/RPM-GPG-KEY-*
/etc/pki/rpm-gpg/RPM-GPG-KEY-unity-29-primary
/etc/pki/rpm-gpg/RPM-GPG-KEY-unity-30-primary
/etc/pki/rpm-gpg/RPM-GPG-KEY-unity-31-primary

%changelog
* Fri Apr 26 2019 JMiahMan <jmiahman@unity-linux.org> 30-0.1
- Release for 30

* Thu Feb 21 2019 JMiahMan <jmiahman@unity-linux.org> 29-0.6
- Ensure rawhide is disabled

* Wed Jan 09 2019 JMiahMan <jmiahman@unity-linux.org> 29-0.5
- Make sure gpg key is added

* Tue Jan 08 2019 JMiahMan <jmiahman@unity-linux.org> 29-0.4
- Update Unity GPG key

* Mon Jan 07 2019 JMiahMan <jmiahman@unity-linux.org> 29-0.3
- Add RPMFusion Repos

* Mon Jan 07 2019 JMiahMan <jmiahman@unity-linux.org> 29-0.2
- Require gpg keys
